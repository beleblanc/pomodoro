#
#  PomodoroController.rb
#  Pomodoro
#
#  Created by Benoit LeBlanc on 2013/06/07.
#  Copyright 2013 Benoit LeBlanc. All rights reserved.

class PomodoroController
    attr_accessor :timer_label, :start_top_button
    attr_accessor :pomodoro_timer, :min_left
    
    def awakeFromNib
       @min_left = 2
    end
    
    def start_stop_timer(sender)
       if @pomodoro_timer.nil?
           @pomodoro_timer = NSTimer.scheduledTimerWithTimeInterval(60,
                                                                    target:self,
                                                                    selector:"min_passed:",
                                                                    userInfo:nil,
                                                                    repeats: true)
           @timer_label.textColor = NSColor.redColor
           @start_top_button.title = "Stop Pomodoro"
       else
           reset_interface
       end
    end
    
    def reset_interface
        @min_left = 2
        @timer_label.stringValue = @min_left
        @timer_label.textColor = NSColor.blackColor
        @start_top_button.title = "Start Timer"
        @pomodoro_timer.invalidate
        @pomodoro_timer = nil
    end
    
    def min_passed(timer)
        if @min_left > 1
            @min_left -=1
            @timer_label.stringValue = @min_left
        else
            reset_interface
            alert_user
        end
    end
    
    def alert_user
        voice = NSSpeechSynthesizer.alloc.initWithVoice("com.apple.speech.synthesis.voice.Victoria")
        voice.startSpeakingString("Pomodoro Complete. Time for a break!")
    end
end
