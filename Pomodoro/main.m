//
//  main.m
//  Pomodoro
//
//  Created by Benoit LeBlanc on 2013/06/07.
//  Copyright (c) 2013 Benoit LeBlanc. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#import <MacRuby/MacRuby.h>

int main(int argc, char *argv[])
{
    return macruby_main("rb_main.rb", argc, argv);
}
